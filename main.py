import PySimpleGUI as sg
import filetype

ZIPFILE=""
sg.theme("DarkTeal2")

def getExport():
    layout = [[sg.T("")], [sg.Text("Pack Zip to convert: "), sg.Input(), sg.FileBrowse()],
          [sg.Button('Submit')]]

    window = sg.Window('My File Browser', layout, size=(600,150))

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event=="Exit":
            break
        elif event == "Submit":
            kind = filetype.guess(values[0])
            alert(kind)
            if kind == 'application/zip':
                ZIPFILE=values[0]
            else:
                alert_notzip()
            break
                    

    window.close()


def alert_notzip():
    err_layout = [[sg.T('Please choose a zip file'),sg.Button('OK')]]

    window = sg.Window('What are you doing?', err_layout)

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event=="Exit":
            break
        elif event == "OK":
            kind = filetype.guess(values[0])
            if kind == 'application/zip':
                NEXTDIALOG ="yes"
            else:
                ERROR="not zip"
            break
                    

    window.close()

def alert(data):
    err_layout = [[sg.T(F'data detected {data}',data),sg.Button('OK')]]

    window = sg.Window('Hmm', err_layout)

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event=="Exit":
            break
        elif event == "OK":
            break

    window.close()

    
def getMeta():
    meta_layout = [
        [sg.T('Pack Name'),sg.Input()],
        [sg.T('Author Name'),sg.Input()],
        [sg.T('Pack Version'),sg.Input()],
        [sg.T('Minecraft Version'),sg.Input()],
        [sg.T('Mod Loader'),sg.DropDown(["Forge","Fabric"])],
        [sg.T('Mod Loader Version'),sg.Input()]
        ]

    window = sg.window('Begin',meta_layout)
    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event=="Exit":
                break

    window.close()

while ZIPFILE=="":
    getExport()
getMeta()

# PackWiz_GUI

## Requirement
**PackWiz** by comp500, the following  is the best way to install it.
*   Install Go (1.17 or newer) from [https://golang.org/dl/](https://golang.org/dl/)
*   Run `go install github.com/packwiz/packwiz@latest`. Be patient, it has to download and compile dependencies as well!

**Alternate** options at https://github.com/packwiz/packwiz

## Description
This is a wrapper written in Python using PySimpleGUI which supports all of Linux, Mac, and Windows. It is my hope that this library supports all the variants of Linux too.

## Bugs and Suggestions
Please use the issue tracker for bugs, suggestions, feedback, etc.

# License
MIT.
